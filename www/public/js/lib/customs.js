(function() {
    /* Mobile Menu */
    const btn = document.querySelector('.menu-icon');
    const menu = document.querySelector('.mobile-menu');
    btn.addEventListener('click', () => {
        btn.classList.toggle('active');
        menu.querySelector('.mobile-overlay').classList.toggle('hidden');
        menu.querySelector('.holder').classList.toggle('active');

        document.querySelectorAll('.link').forEach(el => {
            el.addEventListener('click', () => {
                btn.classList.remove('active');
                menu.querySelector('.mobile-overlay').classList.add('hidden');
                menu.querySelector('.holder').classList.remove('active');
            });
        });
    });
    /* Scroll to */
    document.querySelectorAll('.link').forEach(el => {
        el.addEventListener('click', () => {
            const selector = el.getAttribute('data-nav');
            const targetElement = document.querySelector(`#${selector}`);
            targetElement.scrollIntoView({ behavior: 'smooth' });
        });
    });
    /* Klickstrecke */
    document.querySelectorAll('.get-the-angebote').forEach(el => {
        el.addEventListener('click', () => {
            const loader = document.getElementById('loader');
            loader.style.display = 'block';
            const url = el.getAttribute('data-url');
            const iframe = document.getElementById('angebote');
            iframe.src = url;

            iframe.style.display = 'block';
            setTimeout(() => {
                loader.style.display = 'none';
            }, '650');
        });
    });
})();