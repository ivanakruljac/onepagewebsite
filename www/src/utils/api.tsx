import directusApi from './directusApi'
import neptuneApi from './neptuneApi'

const api = async({ slug }: { slug: string }) => {
    const data: Directus = await directusApi(slug)
    const profileDirectus = data.data[0]
    // -- Get Profile Data From Neptune

    if (profileDirectus) {
        const profile = await neptuneApi.get(`/profiles/${profileDirectus.neptune_id}` )
        .then((res) => {
            return res.data ? res.data : undefined
        })
        .catch(error => console.error(error))
        return (profile)
    }
}

export default api

// how to extract:
// const data = await api(slug)