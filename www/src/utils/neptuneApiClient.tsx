import axios from 'axios'

export const client = axios.create({
    baseURL: process.env.NEXT_PUBLIC_API_URL,
    headers: {
        Authorization: `Bearer ${process.env.NEXT_PUBLIC_API_TOKEN}`
    }
})

client.interceptors.response.use(null, err => {
    // construct new Error
    const e: any = new Error('API Call Failed')
    e.name = 'AxiosError'
    e.stack = err.stack

    if (err.request) {
        e.request = err.request.path
    }

    if (err.response) {
        e.message = `API Call Failed With Status ${err.response.status}`
        e.response = err.response.data
    }

    return Promise.reject(e)
})

export default client