'use client'

import neptune from './neptuneApiClient'
import { useState, useEffect  } from 'react'
import axios from 'axios'

const NeptuneProfile = () => {
    const [data, setData] = useState(null)
    useEffect(() => {
        async function profile() {
            try {
                // const res = await neptune.get('/profiles/5dd1c47451fb36624cb913eb')
                const res = await axios.get('https://swapi.dev/api/people/1/')
                setData(res.data)
            } catch (error) {
                console.error('Error:', error)
            } 
        }
        profile()
    }, [])
    if (!data) return <div>Loading...</div>
    return(data.name)

}

export default NeptuneProfile