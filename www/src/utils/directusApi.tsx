const graphQLAPI = process.env.DIRECTUS_GRAPHQL
const API = process.env.DIRECTUS_API

// -- Get items Response:
// http://localhost:8055/items/profiles
// Filter by slug:
// http://localhost:8055/items/profiles?filter[slug][_eq]=planet-earth
// http://localhost:8055/graphql?query={profiles(filter: {slug: {_eq: "planet-earth" } }){id title slug logo{id} }}

// -- Get Single Profile Data from Direcuts
const directusApi = async(slug: string) => {
    // const res = await fetch(graphQLAPI + `?query={profiles(filter: {slug: {_eq: "${slug}" } }){id vertical neptune_id slug title sub_title business_intro logo{id} seo_text neptune_featured_image meta_title meta_description meta_follow meta_index canonical_url }}`,
    const res = await fetch(API + `?filter[slug][_eq]=${slug}`,
    {
        cache: 'no-store' 
    })
    if (!res.ok) {
        throw new Error('Failed to fetch data')
    }
    return res.json()
}
export default directusApi