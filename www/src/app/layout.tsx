import type { Metadata } from 'next'
import './style/globals.css'
import { Open_Sans } from 'next/font/google'
const os = Open_Sans({ subsets: ['latin'] })
import { cn } from '../lib/utils'

export const metadata: Metadata = {
    title: 'Website Builder',
    description: 'Digitale Seiten Onepage Website Builder',
}

export default function RootLayout({
    children,
}: {
    children: React.ReactNode
}) {
    return (
        <html lang="de">
            <body className={cn(`${os.className}`)}>
                {children}
            </body>
        </html>
    )
}
