import Image from 'next/image'

export default function Home() {
    return (
        <main className="bg-no-repeat bg-center bg-[url('/img/space.jpg')] flex min-h-screen flex-col items-center p-24 bg-slate-200">
            <a className='flex place-items-center gap-2' href='https://www.digitaleseiten.de' target='_blank' rel='noopener noreferrer'>
                <Image src='/img/digitaleseiten.svg' alt='Digitale Seiten Logo' width={292} height={45} priority />
            </a>
            <div className='text-white text-3xl mt-10'>Hello from Space</div>
        </main>
    )
}