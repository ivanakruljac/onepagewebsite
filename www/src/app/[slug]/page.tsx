import directusApi from '../../utils/directusApi'
import neptuneApi from '../../utils/neptuneApi'
import type { Metadata, ResolvingMetadata } from 'next'
import Script from 'next/script'
import Header from '../../components/header'
import Intro from '../../components/intro'
import AboutUs from '../../components/aboutUs'
import Gallery from '../../components/gallery'
import Klickstrecke from '../../components/klickstrecke'
import Ratings from '../../components/ratings'
import Kontakt from '../../components/kontakt'
import Footer from '../../components/footer'
import { notFound } from 'next/navigation'
const assets = process.env.DIRECTUS_ASSETS_URL
// - Fetch data on client side (neptune API do not work): 
// import NeptuneProfile from '../../utils/neptuneClient'

// -- Generate Metadata (this is native Nextjs function and needs to be in page.tsx)
export const generateMetadata = async({ params }: { params: { slug: string } }, parent: ResolvingMetadata): Promise<Metadata> => {
    const data: Directus = await directusApi(params.slug)
    const profileDirectus = data.data[0]

    if (!profileDirectus)
        return {
            title: '404: This page could not be found'
        }

    // -- Get Profile From Neptune
    const profile = await neptuneApi.get(`/profiles/${profileDirectus.neptune_id}` )
    .then((res:any) => {
        return res.data ? res.data : undefined
    })
    .catch(error => console.error(error))

    if (profileDirectus && profile) {
        // -- Directus Featured Image overrides Neptune Featured Image
        let featuredImage: string
        const cloudinaryUrl = 'https://res.cloudinary.com/hxmj4muxr/image/upload'
        if (profile.featured_image) {
            featuredImage = assets + '/' + profile.featured_image
        } else {
            if (profile.media.length) {
                if (profile.media[profileDirectus.neptune_featured_image].cloudinaryId) {
                    featuredImage = cloudinaryUrl + '/v1/' + profile.media[profileDirectus.neptune_featured_image].cloudinaryId
                } else {
                    featuredImage = cloudinaryUrl + '/v1/' + profile.media[profileDirectus.neptune_featured_image].path
                }
            }
        }
        const previousImages = (await parent).openGraph?.images || []
        return {
            title: profileDirectus.meta_title ? profileDirectus.meta_title : 'Profile Title',
            description: profileDirectus.meta_description ? profileDirectus.meta_description : 'Profile Description',
            openGraph: {
                images: [featuredImage, ...previousImages]
            }
        }
    }
}

// -- Get All Profile Data (From Neptune API and Directus)
const profile = async({ params } : { params : { slug: string } }) => {
    const data: Directus = await directusApi(params.slug)
    const profileDirectus = data.data[0]

    if (!profileDirectus) return notFound()

    // -- Get Profile Data From Neptune
    const profile = await neptuneApi.get(`/profiles/${profileDirectus.neptune_id}` )
    .then((res) => {
        return res.data ? res.data : undefined
    })
    .catch(error => console.error(error))

    if (profile) {
        const verticalClass = profile.vertical.substring(0, profile.vertical.indexOf('.'))

        // console.log(profile)

        return (
            <>
                <Script src='./js/app.js' defer strategy='lazyOnload' /> 
                <div className={`${verticalClass} dark:bg-gray-900 dark:text-white`}>
                    {/* Fetch data on client side: */}
                    {/* <NeptuneProfile /> */}

                    <Header slug={params.slug} />

                    <Intro slug={params.slug} />

                    <AboutUs slug={params.slug} />

                    <Gallery slug={params.slug} />

                    <Klickstrecke slug={params.slug} />
                    
                    <Ratings slug={params.slug} />

                    <Kontakt slug={params.slug} />

                    <Footer slug={params.slug} />
                </div>
            </>
        )
    } else 
        return (
            <>
                <h1 className="text-center text-4xl mt-20">{profileDirectus.title}</h1>
                <div className="text-center mt-10 text-2xl">Neptune API is not connected or Neptune ID is false</div>
            </>
        )
}

export default profile