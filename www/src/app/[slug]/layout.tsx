import '../style/globals.css'
import '../style/profile.css'
import '../style/mobilemenu.css'
import '../style/verticals.css'
import { ThemeProvider } from '../../utils/themeProvider'
import { Open_Sans } from 'next/font/google'
const os = Open_Sans({ subsets: ['latin'] })
import { cn } from '../../lib/utils'
import Meta from '../../components/meta'
import Favicons from '../../components/favicons'

export default function RootLayout({
    children, params
}: {
    children: React.ReactNode,
    params: {
        slug: string
    }
}) {
    return (
        <html>
            <head>
                <Meta slug={params.slug} />
                <Favicons slug={params.slug} />
            </head>
            <body className={cn(`${os.className}`)}>
                <ThemeProvider attribute="class" defaultTheme="system" enableSystem>
                    {children}
                </ThemeProvider>
            </body>
        </html>
    )
}