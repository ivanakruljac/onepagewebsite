
import directusApi from '../utils/directusApi'
import neptuneApi from '../utils/neptuneApi'
import Image from 'next/image'
import Script from 'next/script'

const Klickstrecke = async({ slug }: { slug: string }) => {
    const data: Directus = await directusApi(slug)
    const profileDirectus = data.data[0]
    const clientId = profileDirectus.neptune_id
    // -- Get Profile Data From Neptune
    const profile: any = await neptuneApi.get(`/profiles/${clientId}` )
    .then((res) => {
        return res.data ? res.data : undefined
    })
    .catch(error => console.error(error))
    const services = await neptuneApi.get(`/verticals/${profile.vertical}?e=tags`).then(res => res.data.tags)
    const verticalName = profile.vertical.substring(0, profile.vertical.indexOf('.'))

    // todo: create in Directus
    const domain = 'https://www.example.com'

    if (profile.product) {
        return (
            <>
                <div id="klickstrecke" className="klickstrecke">
                    <div className="container">
                        <h2 className="text-center mb-8 lg:mb-16">Angebote kostenlos einholen</h2>
                        <div className="service-grid">
                            {services.filter(s => s.id !== 'sonstiges').map((service: any, index: number) => (
                                <div key={index} className="card get-the-angebote" data-url={`${process.env.BENI_URL}/${verticalName}?service=${service.id}&client_account_id=${clientId}&source=${domain}/${slug}`}>
                                    <Image src={`/img/services/${profile.vertical}/${service.id}.svg`} alt={service.name} width={48} height={48} />
                                    <div className="service">{service.name}</div>
                                </div>
                            ))}
                        </div>
                        <div className="sonstiges get-the-angebote" data-url={`${process.env.BENI_URL}/${verticalName}?service=sonstiges&client_account_id=${clientId}&source=${domain}/${slug}`}>Ich bin nicht sicher, was ich brauche</div>
                    </div>
                </div>
                <div className="modalAngebote" style={{ 'display': 'none' }}>
                    <div id="loader" className="loader">
                        <span></span>
                    </div>
                    <iframe id="angebote" name="angebote" style={{ 'display': 'none' }} src={`${process.env.BENI_URL}/${verticalName}`} width="100%"></iframe>
                </div>
                <Script src='https://dsbeni.herokuapp.com/app/embed.js' defer strategy='lazyOnload' /> 
            </>
        )
    }
}

export default Klickstrecke