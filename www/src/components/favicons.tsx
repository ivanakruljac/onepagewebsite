import api from '../utils/api'

const Favicons = async(slug:any) => {
    // -- Get Profile Data From Neptune
    const profile = await api(slug)

    if (profile) {
    const numbers = [16, 32, 48]
    const sizes = []
    numbers.map((size, index) => {
        sizes.push(<link key={index} rel='icon' href={`https://www.${profile.vertical}/img/favicons/favicon-${size}x${size}.png`} type='image/png' sizes={`${size}x${size}`} />)
    })


        return (sizes)
    }
}

export default Favicons