import directusApi from '../utils/directusApi'
import neptuneApi from '../utils/neptuneApi'
import Stars from '../utils/stars'
import Image from 'next/image'

const Ratings = async({ slug }: { slug: string }) => {
    const data: Directus = await directusApi(slug)
    const profileDirectus = data.data[0]
    // -- Get Profile Ratings From Neptune
    const params = {
        q: { 
            profile: profileDirectus.neptune_id, 
            status: 'approved' 
        },
        limit: 3
    }
    const ratings: any = await neptuneApi.get('/ratings', { params }).then(res => res.data.ratings)
    // -- Get Neptune Profile
    const profile: any = await neptuneApi.get(`/profiles/${profileDirectus.neptune_id}`).then((res) => {
        return res.data ? res.data : undefined
    })
    .catch(error => console.error(error))

    const average = profile.rating.average.toFixed(1)
    const count = profile.rating.count
    const bewertung = (count === 1) ? 'Bewertung' : 'Bewertungen'
    const bewertungNeptune = (profile.rating.count === 1) ? 'Bewertung' : 'Bewertungen'
    const bewertungGoogle = (profile.rating.aggregated.google.countt === 1) ? 'Bewertung' : 'Bewertungen'

    if (ratings.length) {
        let criteriaName: string
        const criteria = ['quality', 'punctuality', 'service', 'price']
        const averageCriteria = []
        criteria.forEach(c => {
            if (c === 'quality') {
                criteriaName = 'Qualität'
            }
            if (c === 'punctuality') {
                criteriaName = 'Pünktlichkeit'
            }
            if (c === 'service') {
                criteriaName = 'Service'
            }
            if (c === 'price') {
                criteriaName = 'Preis/Leistung'
            }
            averageCriteria.push({ 
                value: ratings.map(r => r.criteria[c]).reduce((a, b) => a + b) / ratings.length,
                name: criteriaName 
            })
        })

        return (
            <div id="bewertungen" className="ratings">
                <div className="container">
                    <h2 className="text-center mb-12 lg:mb-16">Kundenbewertungen</h2>
                    <div className="grid lg:grid-cols-12 mb-5">
                        <div className="lg:col-span-3 text-center mb-10">
                            <div className="headline">Gesamtbewertung</div>
                            <div className="summary">
                                <div className="text-4xl font-bold mb-2">{average}</div>
                                <Stars rating={average} /> 
                            </div>
                            <div>basiert auf {count} {bewertung}</div>
                        </div>
                        <div className="lg:col-span-4 xl:pl-6 mb-5">
                            <div className="headline">Bewertungsquellen</div>
                            {profile.rating.count?
                                <div className="source">
                                    <div className="thumb">
                                        <Image src={`https://www.${profile.vertical}/img/favicons/favicon-48x48.png`} alt={''} width={48} height={48} />
                                    </div>
                                    <div>
                                        <div className="average">
                                            <div className="mr-3">
                                                <span className="font-semibold">{profile.rating.average.toFixed(1)}/5</span>
                                            </div>
                                            <Stars rating={profile.rating.average.toFixed(1)} /> 
                                        </div>
                                        <div>bei {profile.rating.count} {bewertungNeptune}</div>
                                    </div>
                                </div>
                            : ''}
                            {profile.rating.aggregated.google.count?
                                <div className="source">
                                    <div className="thumb">
                                        <Image src='/img/google.png' alt='' width={48} height={48} />
                                    </div>
                                    <div>
                                        <div className="average">
                                            <div className="mr-3">
                                                <span className="font-semibold">{profile.rating.aggregated.google.average.toFixed(1)}/5</span>
                                            </div>
                                            <Stars rating={profile.rating.aggregated.google.count.toFixed(1)} /> 
                                        </div>
                                        <div>
                                            <div>bei {profile.rating.aggregated.google.count} {bewertungGoogle}</div>
                                            <div className="text-sm">(nicht in Gesamtbewertung)</div>
                                        </div>
                                    </div>
                                </div>
                            : ''}
                        </div>
                        <div className="lg:col-span-5 lg:pl-10 xl:pl-24 mb-8">
                            <div className="headline-criteria"><span className='pr-1'>Bewertungskriterien auf</span><p>{profile.vertical}</p></div>
                            {averageCriteria.map((c: any, index: number) => (
                                <div className="averageCriteria" key={index}>
                                    <div className="txt">{c.name}</div>
                                    <div className="progress lg:mb-5 mb-2">
                                        <div className="line">
                                            <div className="value" style={{ width: `${c.value * 2 * 10}%` }}></div>
                                        </div>
                                        <span className="text-sm">{c.value.toFixed(1)}/5</span>
                                    </div>
                                </div>
                            ))}
                        </div>
                    </div>
                    <div className="grid">
                        {ratings.map((rate: any, index: number) => (
                            <div className="rate" key={index}>
                                <div className="flex items-center">
                                    <Stars rating={rate.total} /> 
                                    <div className="text-sm pl-3 font-semibold">{rate.total} von 5</div>
                                </div>
                                <div className="name text-prim">{rate.author.name}</div>
                                <div className="text-sm text-gray-600 dark:text-gray-300 mt-1">{(new Date(rate.createdAt)).toLocaleDateString('de-DE')}</div>
                                <div className="criteria">
                                    <div>
                                        <div className="font-semibold text-sm">Qualität</div>
                                        <div className="progress">
                                            <div className="line">
                                                <div className="value" style={{ width: `${rate.criteria.quality * 2 * 10}%` }}></div>
                                            </div>
                                            <span className="text-sm">{rate.criteria.quality}/5</span>
                                        </div>
                                    </div>
                                    <div>
                                        <div className="font-semibold text-sm">Pünktlichkeit</div>
                                        <div className="progress">
                                            <div className="line">
                                                <div className="value" style={{ width: `${rate.criteria.punctuality * 2 * 10}%` }}></div>
                                            </div>
                                            <span className="text-sm">{rate.criteria.punctuality}/5</span>
                                        </div>
                                    </div>
                                    <div>
                                        <div className="font-semibold text-sm">Service</div>
                                        <div className="progress">
                                            <div className="line">
                                                <div className="value" style={{ width: `${rate.criteria.service * 2 * 10}%` }}></div>
                                            </div>
                                            <span className="text-sm">{rate.criteria.service}/5</span>
                                        </div>
                                    </div>
                                    <div>
                                        <div className="font-semibold text-sm">Preis/Leistung</div>
                                        <div className="progress">
                                            <div className="line">
                                                <div className="value" style={{ width: `${rate.criteria.price * 2 * 10}%` }}></div>
                                            </div>
                                            <span className="text-sm">{rate.criteria.price}/5</span>
                                        </div>
                                    </div>
                                </div>
                                <div className="title text-prim">{rate.title}</div>
                                <div className="text-md">{rate.body}</div>
                            </div>
                        ))}
                    </div>
                </div>
            </div>
        )
    }
}

export default Ratings