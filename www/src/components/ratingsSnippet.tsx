import directusApi from '../utils/directusApi'
import neptuneApi from '../utils/neptuneApi'
import Stars from '../utils/stars'
import Image from 'next/image'

const RatingsSnippet = async({ slug }: { slug: string }) => {
    const data: Directus = await directusApi(slug)
    const profileDirectus = data.data[0]
    // -- Get Profile Ratings From Neptune
    const params = {
        q: { 
            profile: profileDirectus.neptune_id, 
            status: 'approved' 
        },
        limit: 3
    }
    const ratings: any = await neptuneApi.get('/ratings', { params }).then(res => res.data.ratings)
    // -- Get Neptune Profile
    const profile: any = await neptuneApi.get(`/profiles/${profileDirectus.neptune_id}`).then((res) => {
        return res.data ? res.data : undefined
    })
    .catch(error => console.error(error))

    const average = profile.rating.average.toFixed(1)

    if (ratings.length) {
        return (
            <div className="starsnippet">
                <Stars rating={average} />
            </div>
        )
    }
}

export default RatingsSnippet