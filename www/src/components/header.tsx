import directusApi from '../utils/directusApi'
import neptuneApi from '../utils/neptuneApi'
const assets = process.env.DIRECTUS_ASSETS_URL
import Image from 'next/image'
import Link from 'next/link'
import { ThemeToggle } from './themeToggle'

const Header = async ({ slug }: { slug: string }) => {
    const data: Directus = await directusApi(slug)
    const profileDirectus = data.data[0]
    const profile: any = await neptuneApi.get(`/profiles/${profileDirectus.neptune_id}` )
    .then((res) => {
        return res.data ? res.data : undefined
    })
    .catch(error => console.error(error))
    return (
        <>
            <header>
                <div className="container">
                    <Link className='flex items-center' href="/">
                        { profileDirectus.logo ?
                            <Image src={`${assets}/${profileDirectus.logo}`} alt={profileDirectus.title} width={300} height={40} style={{ width:'auto', height:'40px' }} className="dark:invert"/>
                        :
                            <div className="text-2xl font-bold">Logo</div>
                        }
                    </Link>
                    <div className="flex items-center lg:hidden">
                        <span className="mr-3"><ThemeToggle/></span>
                        <div className="menu-icon"><span></span></div>
                    </div>
                    <div className="w-full lg:block lg:w-auto hidden">
                        <ul className="flex flex-col lg:flex-row">
                            <li className="hidden md:block"><ThemeToggle /></li>
                            <li><div className="link" data-nav="ueber-uns">Über uns</div></li>
                            {profile.media.length ?
                                <li><div className="link" data-nav="galerie">Galerie</div></li>
                            : ''}
                            {profile.product ?
                                <li><div className="link" data-nav="klickstrecke">Angebote</div></li>
                            : ''}
                            {profile.rating.count || profile.rating.aggregated.google.count ?
                                <li><div className="link" data-nav="bewertungen">Bewertungen</div></li>
                            : ''}
                            <li><div className="link" data-nav="kontakt">Kontakt</div></li>
                        </ul>
                    </div>
                </div>
            </header>
            <div className="mobile-menu">
                <div className="mobile-overlay hidden"></div>
                <div className="holder">
                    <div className="link" data-nav="ueber-uns">Über uns</div>
                    {profile.media.length ?
                        <div className="link" data-nav="galerie">Galerie</div>
                    : ''}
                    {profile.product ?
                        <div className="link" data-nav="klickstrecke">Angebote</div>
                    : ''}
                    {profile.rating.count || profile.rating.aggregated.google.count ?
                        <div className="link" data-nav="bewertungen">Bewertungen</div>
                    : ''}
                    <div className="link" data-nav="kontakt">Kontakt</div>
                </div>
            </div>
        </>
    )
}

export default Header