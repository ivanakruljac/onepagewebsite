
import directusApi from '../utils/directusApi'
import neptuneApi from '../utils/neptuneApi'
import RatingsSnippet from '../components/ratingsSnippet'
import ModalCallUs from '../components/callUs'
import { Phone } from 'lucide-react'

const Intro = async ({ slug }: { slug: string }) => {
    const data: Directus = await directusApi(slug)
    const profileDirectus = data.data[0]
    const profile: any = await neptuneApi.get(`/profiles/${profileDirectus.neptune_id}` )
    .then((res) => {
        return res.data ? res.data : undefined
    })
    .catch(error => console.error(error))
    return (
        <div className="intro">
            <div className="container">
                <div className="jumbotron">
                    <div className="mb-4 text-prim font-bold">{profileDirectus.business_intro}</div>
                    <h1>{profileDirectus.title}</h1>
                    <RatingsSnippet slug={slug} />
                    <div className="subtitle">{profileDirectus.sub_title}</div>
                    <div className="btns-hold">
                        { profile.contact.phone ?
                            <a href={`tel:${profile.contact.phone}`} className="btn btn-primary btn-call">
                                <Phone />
                                <span>Direkt Anrufen</span>
                                {profile.contact.phone}
                            </a>
                        : ''}
                        <ModalCallUs slug={slug} />
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Intro