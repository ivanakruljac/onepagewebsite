import api from '../utils/api'
import Image from 'next/image'
import Link from 'next/link'

const Footer = async (slug:any) => {
    const profile = await api(slug)
    return (
        <footer>
        <div className="container">
            <div className="hold">
                <Link href={`https://www.${profile.vertical}`} target="blank">
                    <Image src={`https://www.${profile.vertical}/img/logo.svg`} alt={profile.vertical} className="dark:filter dark:grayscale mx-auto" width={0} height={40} style={{ width:'auto', height:'40px' }} /> 
                </Link>
                <div className="mt-3 text-sm">© Ein Portal der Digitale Seiten. Alle Rechte vorbehalten.</div>
            </div>
        </div>
    </footer>
    )
}

export default Footer