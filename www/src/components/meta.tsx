import directusApi from '../utils/directusApi'
import neptuneApi from '../utils/neptuneApi'
import type { Metadata, ResolvingMetadata } from 'next'
const assets = process.env.DIRECTUS_ASSETS_URL

const Meta = async ({ slug }: { slug: string }) => {
    const data: Directus = await directusApi(slug)
    const profile = data.data[0]

    if (profile) {
        let index = (profile.meta_index === true)? 'index' : 'noindex'
        let follow = (profile.meta_follow === true)? 'follow' : 'nofollow'
        let canonical = profile.canonical_url? profile.canonical_url : (profile.live_domain? profile.live_domain : 'https//:www.example.com') 

        return (
            <>
                <meta name='robots' content={`${index}, ${follow}`} />
                <link rel="canonical" href={canonical} />
            </>
        )
    }
}

export default Meta