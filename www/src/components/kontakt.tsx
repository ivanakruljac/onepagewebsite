import directusApi from '../utils/directusApi'
import neptuneApi from '../utils/neptuneApi'
import { Phone, MapPin } from 'lucide-react'
import PatternMin from '../../public/svg/patternmin'
import FormContactUs from './forms/formContactUs'
const assets = process.env.DIRECTUS_ASSETS_URL

const Kontakt = async({ slug }: { slug: string }) => {
    const data: Directus = await directusApi(slug)
    const profileDirectus = data.data[0]
    // -- Get Profile Data From Neptune
    const profile: any = await neptuneApi.get(`/profiles/${profileDirectus.neptune_id}` )
    .then((res) => {
        return res.data ? res.data : undefined
    })
    .catch(error => console.error(error))

    const domain = profileDirectus.live_domain || 'https://www.example.com'

    return (
        <div id="kontakt" className="contact">
            <span className="pattern-top"><PatternMin /></span>
            <span className="pattern-bottom"><PatternMin /></span>
            <div className="container">
                <h2 className="text-center mb-12 lg:mb-16">In Kontakt treten</h2>
                <div className={profile.businessHours.length ? 'grid gap-10 lg:grid-cols-3' : 'grid gap-10 lg:grid-cols-2'}>
                    <div>
                        <div className="text-lg font-bold dark:font-semibold mb-4">Kontakt</div>
                        <div className="info">
                            <Phone />
                            {profile.contact.phone}
                        </div>
                        <div className="info">
                            <MapPin />
                            {profile.address.street}, {profile.address.zipcode} {profile.address.city}
                        </div>
                        <a className="btn btn-primary-ghost mt-3" href={'https://maps.google.com/?q='+ profile.address.street + ',' + profile.address.zipcode + profile.address.city } target='_blank' rel='nofollow'>Anfahrt plannen</a>
                    </div>
                    { profile.businessHours.length ?
                        <div className='lg:pl-10'>
                            <div className="text-lg font-bold dark:font-semibold mb-4">Öffnungszeiten</div>
                            { profile.businessHours.map((time : { day: string, from: number, to: number, text: string }, index) => (
                                <div className="workinghours" key={index}>
                                    <span>
                                        {time.day === 'mon' ? 'Montag' : ''}
                                        {time.day === 'tue' ? 'Dienstag' : ''}
                                        {time.day === 'wed' ? 'Mittwoch' : ''}
                                        {time.day === 'thu' ? 'Donnerstag' : ''}
                                        {time.day === 'fri' ? 'Freitag' : ''}
                                        {time.day === 'sat' ? 'Samstag' : ''}
                                        {time.day === 'sun' ? 'Sonntag' : ''}
                                        {time.day === 'holiday' ? 'Feiertag' : ''}
                                    :</span>
                                    {time.from? time.from + ' - ' : ''}{time.to? time.to + ' ' : ''}{time.text? time.text : ''}
                                </div>
                            )) }
                        </div>
                    : ''}
                    <div className="max-w-[450px] w-full mx-auto">
                        <div className="text-lg font-bold dark:font-semibold mb-4">Ihre Nachricht an uns</div>
                        <FormContactUs 
                            domain={domain} 
                            company={profileDirectus.title}
                            logo={`${assets}/${profileDirectus.logo}`}
                            vertical={profile.vertical} />
                   </div>
                </div>
            </div>
        </div>
    )
}

export default Kontakt