import directusApi from '../utils/directusApi'
import neptuneApi from '../utils/neptuneApi'
import Image from 'next/image'
import Pattern from '../../public/svg/pattern'
const assets = process.env.DIRECTUS_ASSETS_URL

const AboutUs = async ({ slug }: { slug: string }) => {
    const data: Directus = await directusApi(slug)
    const profileDirectus = data.data[0]
    const profile: any = await neptuneApi.get(`/profiles/${profileDirectus.neptune_id}` )
    .then((res) => {
        return res.data ? res.data : undefined
    })
    .catch(error => console.error(error))

    const cloudinaryUrl = 'https://res.cloudinary.com/hxmj4muxr/image/upload'

    // -- Directus SEOtext overrides Neptune SEOtext
    let seoText: string
    if (profileDirectus.seo_text) {
        seoText = profileDirectus.seo_text
    } else {
        seoText = profile.description
    }

    // -- Directus Featured Image overrides Neptune Featured Image
    let featuredImage: string
    let featuredImageThumb: string
    if (profileDirectus.featured_image) {
        featuredImage = featuredImageThumb = assets + '/' + profileDirectus.featured_image
    } else {
        if (profile.media.length) {
            if (profile.media[profileDirectus.neptune_featured_image].cloudinaryId) {
                featuredImage = cloudinaryUrl + '/v1/' + profile.media[profileDirectus.neptune_featured_image].cloudinaryId
                featuredImageThumb = cloudinaryUrl + '/c_fill,h_420,w_370/v1/' + profile.media[profileDirectus.neptune_featured_image].cloudinaryId   
            } else {
                featuredImage = cloudinaryUrl + '/v1/' + profile.media[profileDirectus.neptune_featured_image].path
                featuredImageThumb = cloudinaryUrl + '/c_fill,h_420,w_370/v1/' + profile.media[profileDirectus.neptune_featured_image].path
            }
        }
    }

    if (seoText) {
        return (
            <div id="ueber-uns" className={`about ${profile.media.length? '' : 'mb-20' }`}>
                <div className="container">
                    <h2 className="lg:text-left text-center mb-8 lg:mb-16">Über uns</h2>
                    <div className="lg:grid lg:grid-cols-3 gap-6 lg:gap-12">
                        <div className="col-span-2 lg:pr-10">
                            <div className="seotext" dangerouslySetInnerHTML={{ __html: seoText }} />
                        </div>
                        { featuredImage ?
                            <figure>
                                <div className="hold">
                                    <div className="spotlight cursor-pointer" data-src={featuredImage}>
                                        <Image src={featuredImageThumb} alt={profileDirectus.title} width={370} height={420} priority />
                                    </div>
                                </div>
                                <span><Pattern /></span>
                            </figure>
                        : ''}
                    </div>
                </div>
            </div>
        )
    }
}

export default AboutUs