import directusApi from '../utils/directusApi'
import neptuneApi from '../utils/neptuneApi'
import {
    Dialog,
    DialogClose,
    DialogContent,
    DialogDescription,
    DialogHeader,
    DialogTitle,
    DialogTrigger,
} from './ui/dialog'
import FormCallUs from './forms/formCallUs'
const assets = process.env.DIRECTUS_ASSETS_URL

const ModalCallUs = async({ slug }: { slug: string }) => {
    const data: Directus = await directusApi(slug)
    const profileDirectus = data.data[0]
    const profile: any = await neptuneApi.get(`/profiles/${profileDirectus.neptune_id}` )
    .then((res) => {
        return res.data ? res.data : undefined
    })
    .catch(error => console.error(error))
    const verticalClass = profile.vertical.substring(0, profile.vertical.indexOf('.'))

    const domain = profileDirectus.live_domain || 'https://www.example.com'

    return (
        <Dialog>
            <DialogTrigger asChild>
                <div className="btn btn-primary-ghost">Rückruf Anforden</div>
            </DialogTrigger>
            <DialogContent className={`${verticalClass} callus`}>
                <DialogHeader>
                    <DialogTitle className="text-2xl text-center">Rückruf anfordern</DialogTitle>
                    <DialogDescription className="text-xl text-center dark:text-gray-300">
                        Wir beraten Sie gerne!
                    </DialogDescription>
                </DialogHeader>
                <div className="py-4">
                    <FormCallUs 
                        domain={domain} 
                        company={profileDirectus.title}
                        logo={`${assets}/${profileDirectus.logo}`}
                        vertical={profile.vertical} />
                    <DialogClose asChild><div className="cursor-pointer hover:underline">Cancel</div></DialogClose>
                </div>
            </DialogContent>
        </Dialog>
    )
}

export default ModalCallUs