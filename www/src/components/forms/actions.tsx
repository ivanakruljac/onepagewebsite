'use server'
import mandrill from 'mandrill-api/mandrill'
const mandrillClient = new mandrill.Mandrill(process.env.MANDRILL_API_KEY)
import { revalidatePath } from 'next/cache'
import z from 'zod'

let today = new Date()
// example format: 05.05.2024
let date = ('0' + today.getDate()).slice(-2) + '.' + ('0' + (today.getMonth() + 1)).slice(-2) + '.' + today.getFullYear()
// example format: 09:09
let time = ('0' + today.getHours()).slice(-2) + ':' + ('0' + today.getMinutes()).slice(-2)

const phoneRegex = new RegExp(/^[0-9\s]{7,16}/)

// -- Handle callUs Form:
export async function callus(prevState: any, formData: FormData) {
    const schema = z.object({
        name: z.string().trim().min(3, "Name is Pflichtfeld").max(50, "Maximallänge für die Name ist 50 Zeichen"),
        phone: z.string().trim().regex(phoneRegex, "Bitte geben Sie eine gültige Telefonnummer an"),
        domain: z.string().trim().max(40),
        company: z.string().trim().max(40),
        logo: z.string().trim().min(10).max(100),
        vertical: z.string().trim().min(5).max(40)
    })

    // -- Honeypot:
    if (formData.get('lastname')) {
        return { message: '<div class="error">Nachricht wurde nicht gesendet.<br/>Bitte versuchen Sie nochmal.</div>' }
    }

    try {

        const data = schema.parse({
            name: formData.get('name'),
            phone: formData.get('phone'),
            domain: formData.get('domain'),
            company: formData.get('company'),
            logo: formData.get('logo'),
            vertical: formData.get('vertical'),
        })

        // -- Send Email to Profile via Mandrill:
        mandrillClient.messages.sendTemplate({
            template_name: 'onepage-website-callus',
            template_content: [],
            message: {
                from_email: 'info@digitaleseiten.de',
                from_name: 'Digitale Seiten',
                subject: 'Rückruf angefordert',
                to: [{
                    type: 'to',
                    email: 'kruljac.ivana@gmail.com'
                }],
                global_merge_vars: [
                    { name: 'DATE', content: date },
                    { name: 'TIME', content: time },
                    { name: 'DOMAIN', content: data.domain },
                    { name: 'LOGO', content: data.logo },
                    { name: 'VERTICAL_LINK', content: `https://www.${data.vertical}`},
                    { name: 'VERTICAL_NAME', content: data.vertical },
                    { name: 'COMPANY', content: data.company },
                    { name: 'NAME', content: data.name },
                    { name: 'PHONE', content: `+${data.phone}` }
                ],
            },
        })
        revalidatePath('/')
        return { message: '<div class="success">Nachricht wurde erfolgreich gesendet!<svg class="checkmark" xmlns="http://www.w3.org/2000/svg" width="80" height="80" viewBox="0 0 52 52"><circle class="c_circle" cx="26" cy="26" r="25" fill="none"/><path class="c_check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8"/></svg></div>' }
    } catch (e) {
        const zerr = e.errors
        if (zerr) return { err: zerr }
        return { message: '<div class="error">Nachricht wurde nicht gesendet.<br/>Bitte versuchen Sie nochmal.</div>' }
    }
}

// -- Handle contactus Form:
export async function contactus(prevState: any, formData: FormData) {

    const schema = z.object({
        email: z.string().email("Bitte geben Sie eine gültige E-Mail-Adresse an").trim(),
        text: z.string().trim().min(10, "Mindestlänge für die Nachricht ist 10 Zeichen").max(500, "Maximallänge für die Nachricht ist 500 Zeichen"),
        domain: z.string().trim().max(40),
        company: z.string().trim().max(40),
        logo: z.string().trim().min(10).max(100),
        vertical: z.string().trim().min(5).max(40)
    })

    // -- Honeypot:
    if (formData.get('lastname')) {
        return { message: '<div class="error">Nachricht wurde nicht gesendet.<br/>Bitte versuchen Sie nochmal.</div>' }
    }

    try {
        const data = schema.parse({
            email: formData.get('email'),
            text: formData.get('text'),
            domain: formData.get('domain'),
            company: formData.get('company'),
            logo: formData.get('logo'),
            vertical: formData.get('vertical'),
        })

        // -- Send Email to Profile via Mandrill:
        // mandrillClient.messages.sendTemplate({
        //     template_name: 'onepage-website-contactus',
        //     template_content: [],
        //     message: {
        //         from_email: 'info@digitaleseiten.de',
        //         from_name: 'Digitale Seiten',
        //         subject: 'Nachricht erhalten',
        //         to: [{
        //             type: 'to',
        //             email: 'kruljac.ivana@gmail.com'
        //         }],
        //         global_merge_vars: [
        //             { name: 'DATE', content: date },
        //             { name: 'TIME', content: time },
        //             { name: 'DOMAIN', content: data.domain },
        //             { name: 'LOGO', content: data.logo },
        //             { name: 'VERTICAL_LINK', content: `https://www.${data.vertical}` },
        //             { name: 'VERTICAL_NAME', content: data.vertical },
        //             { name: 'COMPANY', content: data.company },
        //             { name: 'EMAIL', content: data.email },
        //             { name: 'TEXT', content: data.text }
        //         ],
        //     },
        // })
        revalidatePath('/')
        return { message: '<div class="success">Nachricht wurde erfolgreich gesendet!</div>' }
    } catch (e) {
        const zerr = e.errors
        if (zerr) return { err: zerr }
        return { message: '<div class="error">Nachricht wurde nicht gesendet.<br/>Bitte versuchen Sie es nochmal.</div>' }
    }

}