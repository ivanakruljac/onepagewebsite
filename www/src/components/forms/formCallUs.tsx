'use client'
import { callus } from './actions'
import { useFormStatus } from 'react-dom'
import { useFormState } from 'react-dom'

const initialState = {
    message: null
}

function SubmitButton() {
    const { pending } = useFormStatus()
    
    return (
        <button type="submit" disabled={pending} className="mt-5 w-full btn btn-primary dark:border-slate-400 dark:border">
            {pending ? "Submitting..." : "Rückruf Anforden" }
        </button>
    )
}

const FormCallUs = (params:any) => {
    const [state, formAction] = useFormState(callus, initialState)
    
    return (
        <>
            <form action={formAction}>
                <input type="hidden" name="domain" value={params.domain} />
                <input type="hidden" name="company" value={params.company} />
                <input type="hidden" name="logo" value={params.logo} />
                <input type="hidden" name="vertical" value={params.vertical} />
                <div className="mb-5">
                    <label htmlFor='name' className="mb-2 block text-left">Vorname und Nachname</label>
                    <input type='text' name='name' id='name' placeholder='Max Mustermann' required />
                    <input type="text" name='lastname' id='lastname' placeholder='Nachname' className="mt-2 hidden" />
                </div>
                <div className="mb-5 phone">
                    <label htmlFor='phone' className="mb-2 block text-left">Telefonnummer</label>
                    <input type="tel" name='phone' id='phone' placeholder='495206588444' required />
                </div>
                <div className="my-6">
                    <SubmitButton />
                </div>
            </form>
            {state?.err?
                <div className="s-error">
                    {state.err.map((e:any, index:number) => (
                        <div key={index} className="mb-1 text-sm">{index + 1}. {e.message}.</div>
                    ))}
                </div>
            : ''}
            {state?.message?
                <div className="response" dangerouslySetInnerHTML={{ __html: state.message}}></div>
            : ''}
        </>
    )
}

export default FormCallUs
