'use client'
import { contactus } from './actions'
import { useFormStatus } from 'react-dom'
import { useFormState } from 'react-dom'
import { useRef } from 'react'

const initialState = {
    message: null
}

function SubmitButton() {
    const { pending } = useFormStatus()
    
    return (
        <button type="submit" disabled={pending} className="mt-3 w-full btn btn-primary dark:border-slate-400 dark:border">
            {pending ? "Submitting..." : "Jetzt kontaktieren" }
        </button>
    )
}

const FormContactUs = (params:any) => {
    const [state, formAction] = useFormState(contactus, initialState)
    const ref = useRef<HTMLFormElement>(null)
    // -- we use ref to reset form after submit
    const resetForm = state?.message? ref.current?.reset() : ''
    return (
        <>
            <form ref={ref}
                action={async (formData) => {
                    await formAction(formData)
                    resetForm
                }}>
                <input type="hidden" name="domain" value={params.domain} />
                <input type="hidden" name="company" value={params.company} />
                <input type="hidden" name="logo" value={params.logo} />
                <input type="hidden" name="vertical" value={params.vertical} />
                <div className="mb-5">
                    <label htmlFor='email' className="mb-2 block text-left dark:text-slate-400">E-Mail-Addresse</label>
                    <input type='email' name='email' id='email' placeholder='Bitte geben Sie hier Ihre E-Mail-Adresse an' required />
                    <input type="text" name='lastname' id='lastname' placeholder='Nachname' className="mt-2 hidden" />
                </div>
                <div className="mb-5">
                    <label htmlFor='text' className="mb-2 block text-left dark:text-slate-400">Nachricht</label>
                    <textarea name='text' id='text' placeholder='Bite geben Sie Ihre Nachricht an uns an' required></textarea>
                </div>
                <div className="mb-6">
                    <SubmitButton />
                </div>
            </form>
            {state?.err?
                <div className="s-error">
                    {state.err.map((e:any, index:number) => (
                        <div key={index} className="mb-1 text-sm">{index + 1}. {e.message}.</div>
                    ))}
                </div>
            : ''}
            {state?.message?
                <div className="response" dangerouslySetInnerHTML={{ __html: state.message}}></div>
            : ''}
        </>
    )
}

export default FormContactUs