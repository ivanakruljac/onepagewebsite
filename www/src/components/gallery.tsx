import directusApi from '../utils/directusApi'
import neptuneApi from '../utils/neptuneApi'
import Image from 'next/image'

const Gallery = async({ slug }: { slug: string }) => {
    const data: Directus = await directusApi(slug)
    const profileDirectus = data.data[0]
    // -- Get Profile Data From Neptune
    const profile: any = await neptuneApi.get(`/profiles/${profileDirectus.neptune_id}` )
    .then((res) => {
        return res.data ? res.data : undefined
    })
    .catch(error => console.error(error))
    const cloudinaryUrl = 'https://res.cloudinary.com/hxmj4muxr/image/upload'

    if (profile.media.length) {
        return (
            <div id="galerie" className="gallery">
                <div className="container">
                    <h2 className="text-center mb-8 lg:mb-16">Arbeitsexemplare</h2>
                    <div className="slide">
                        {profile.media.slice(0, 8).map((image: any, index: number) => (
                            <figure key={index}>
                                {image.cloudinaryId ?
                                    <div className="spotlight cursor-pointer" data-src={`${cloudinaryUrl}/v1/${image.cloudinaryId}`}>
                                        <Image src={`${cloudinaryUrl}/c_fill,w_400,h_300/v1/${image.cloudinaryId}`} alt={profileDirectus.title} width={400} height={300} />
                                    </div>
                                    :
                                    <div className="spotlight cursor-pointer" data-src={`${cloudinaryUrl}/v1/${image.path}`}>
                                        <Image src={`${cloudinaryUrl}/c_fill,w_400,h_300/v1/${image.path}`} alt={profileDirectus.title} width={400} height={300} />
                                    </div>
                                }
                            </figure>
                        ))}
                    </div>
                </div>
            </div>
        )
    }
}

export default Gallery