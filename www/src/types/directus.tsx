type Directus = {
    data: {
        id: number,
        neptune_id: number,
        slug: string,
        title: string,
        domain: string,
        vertical: string
    }
}

// todo:
// type Neptune = {
//     data: {
//         id: number,
//         slug: string,
//         title: string
//     }
// }