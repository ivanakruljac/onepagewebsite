/** @type {import('next').NextConfig} */
const nextConfig = {
    images: {
        remotePatterns: [
            {
                protocol: 'http',
                hostname: 'localhost',
                port: '8055',
                pathname: '/assets/**',
            },{
                protocol: 'https',
                hostname: 'res.cloudinary.com',
            },{
                protocol: 'https',
                hostname: 'www.akustiker.net',
            },{
                protocol: 'https',
                hostname: 'www.augenoptiker-finden.de',
            },{
                protocol: 'https',
                hostname: 'www.auto-werkstatt.de',
            },{
                protocol: 'https',
                hostname: 'www.bauunternehmen.org',
            },{
                protocol: 'https',
                hostname: 'www.bodenleger.net',
            },{
                protocol: 'https',
                hostname: 'www.dachdecker.com',
            },{
                protocol: 'https',
                hostname: 'www.elektriker.org',
            },{
                protocol: 'https',
                hostname: 'www.fahrradreparatur.org',
            },{
                protocol: 'https',
                hostname: 'www.fensterbau.org',
            },{
                protocol: 'https',
                hostname: 'www.finanzberater.net',
            },{
                protocol: 'https',
                hostname: 'www.fliesenleger.net',
            },{
                protocol: 'https',
                hostname: 'www.gartenbau.org',
            },{
                protocol: 'https',
                hostname: 'www.geruestbau.org',
            },{
                protocol: 'https',
                hostname: 'www.glaserei.org',
            },{
                protocol: 'https',
                hostname: 'www.gutachter.org',
            },{
                protocol: 'https',
                hostname: 'www.hausmeisterdienste.net',
            },{
                protocol: 'https',
                hostname: 'www.heizungsbau.net',
            },{
                protocol: 'https',
                hostname: 'www.kaminbau.net',
            },{
                protocol: 'https',
                hostname: 'www.klimatechniker.net',
            },{
                protocol: 'https',
                hostname: 'www.maler.org',
            },{
                protocol: 'https',
                hostname: 'www.sanimio.de',
            },{
                protocol: 'https',
                hostname: 'www.sanitaer.org',
            },{
                protocol: 'https',
                hostname: 'www.schaedlingsvernichtung.de',
            },{
                protocol: 'https',
                hostname: 'www.schlosserei.net',
            },{
                protocol: 'https',
                hostname: 'www.steuer-berater.de',
            },{
                protocol: 'https',
                hostname: 'www.tischler-schreiner.org',
            },{
                protocol: 'https',
                hostname: 'www.trockenbauunternehmen.net',
            }
        ],
    }
}

module.exports = nextConfig
