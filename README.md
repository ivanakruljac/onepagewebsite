# One Page Builder

Small one page website builder

## Tech Stack

### cms
- Directus as headless CMS (REST API, GraphQL) - Set up with Docker
### www
- Next.js 14 (with Tailwind)

## Setup Steps:
```
$ git clone git@code.digitaleseiten.net:digitaleseiten/website.git
```

## Dev mode:
Start Directus (cms), Spin up the Docker:
```
$ cd website/cms
$ docker-compose up -d
```
localhost: http://localhost:8055/admin/login

Start Next.js (www)
```
$ cd website/www
$ npm run dev
```

### Libraries
- Tailwind CSS
- Shadcn UI